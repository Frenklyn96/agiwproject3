from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
from htmldom import htmldom
import requests
import hashlib
import random
import time
from selenium.webdriver.common.keys import Keys
import pickle
import tegola
import maschereAndClustering



def conteggi (mascherapath,clusteringpath,dbpath):
    cont=0
    a_file=open(mascherapath, "rb")
    maschere = pickle.load(a_file)
    a_file.close()
    a_file=open(clusteringpath, "rb")
    cluster = pickle.load(a_file)
    a_file.close()
    a_file=open(dbpath, "rb")
    db = pickle.load(a_file)
    a_file.close()
    for x in db:
        mascherasito=[]
        flag=0
        for y in cluster.keys():
            if(x in cluster[y]):
                mascherasito.append(y)
        for z in maschere:
            if (tuple(z) in mascherasito)and (flag==0):
                cont+=1
                flag=1
    return (cont)

def contaSiti (pathfile):
    a_file=open(pathfile, "rb")
    datast= pickle.load(a_file)
    a_file.close()
    return(len(datast))
#Train
'''
maschereAndClustering.diviTestAndSet("dataset/siti/themoviedbSerieTv.pkl","dataset/siti/test/testThemoviedbSerieTv.pkl","dataset/siti/train/trainThemoviedbSerieTv.pkl")
maschereAndClustering.diviTestAndSet("dataset/siti/discotecalaziale.pkl","dataset/siti/test/testDiscotecalaziale.pkl","dataset/siti/train/trainDiscotecalaziale.pkl")
maschereAndClustering.diviTestAndSet("dataset/siti/themoviedbPersone.pkl","dataset/siti/test/testThemoviedbPersone.pkl","dataset/siti/train/trainthemoviedbPersone.pkl")


maschereAndClustering.trovaMaschere("dataset/siti/train/trainThemoviedbSerieTv.pkl","dataset/maschere/maschereTrainThemoviedbSerieTv.pkl")
maschereAndClustering.trovaMaschere("dataset/siti/train/trainDiscotecalaziale.pkl","dataset/maschere/maschereTrainDiscotecalaziale.pkl")
maschereAndClustering.trovaMaschere("dataset/siti/train/trainThemoviedbPersone.pkl","dataset/maschere/maschereTrainThemoviedbPersone.pkl")
maschereAndClustering.clusterizzazione("dataset/maschere/maschereTrainDiscotecalaziale.pkl","dataset/siti/train/trainDiscotecalaziale.pkl","dataset/clustering/clustering.pkl")
maschereAndClustering.clusterizzazione("dataset/maschere/maschereTrainThemoviedbSerieTv.pkl","dataset/siti/train/trainThemoviedbSerieTv.pkl","dataset/clustering/clustering.pkl")
maschereAndClustering.clusterizzazione("dataset/maschere/maschereTrainThemoviedbPersone.pkl","dataset/siti/train/trainThemoviedbPersone.pkl","dataset/clustering/clustering.pkl")
'''
#Test
'''
maschereAndClustering.trovaMaschere("dataset/siti/test/testDiscotecalaziale.pkl","dataset/maschere/maschereTestDiscotecalaziale.pkl")
maschereAndClustering.trovaMaschere("dataset/siti/test/testThemoviedbSerieTv.pkl","dataset/maschere/maschereTestThemoviedbSerieTv.pkl")
maschereAndClustering.trovaMaschere("dataset/siti/test/testThemoviedbPersone.pkl","dataset/maschere/maschereTestThemoviedbPersone.pkl")
maschereAndClustering.clusterizzazione("dataset/maschere/maschereTestDiscotecalaziale.pkl","dataset/siti/test/testDiscotecalaziale.pkl","dataset/clustering/clusteringTest.pkl")
maschereAndClustering.clusterizzazione("dataset/maschere/maschereTestThemoviedbSerieTv.pkl","dataset/siti/test/testThemoviedbSerieTv.pkl","dataset/clustering/clusteringTest.pkl")
maschereAndClustering.clusterizzazione("dataset/maschere/maschereTestThemoviedbPersone.pkl","dataset/siti/test/testThemoviedbPersone.pkl","dataset/clustering/clusteringTest.pkl")
'''

errori1=conteggi("dataset/maschere/maschereTrainDiscotecalaziale.pkl","dataset/clustering/clusteringTest.pkl","dataset/siti/test/testThemoviedbSerieTv.pkl")
print("Flaso positivo: ",errori1)

giusti1=conteggi("dataset/maschere/maschereTrainThemoviedbSerieTv.pkl","dataset/clustering/clusteringTest.pkl","dataset/siti/test/testThemoviedbSerieTv.pkl")
print("Vero positvio: ",giusti1)

errori2=conteggi("dataset/maschere/maschereTrainThemoviedbSerieTv.pkl","dataset/clustering/clusteringTest.pkl","dataset/siti/test/testThemoviedbPersone.pkl")
print("Flaso positivo: ",errori2)

giusti2=conteggi("dataset/maschere/maschereTrainThemoviedbPersone.pkl","dataset/clustering/clusteringTest.pkl","dataset/siti/test/testThemoviedbPersone.pkl")
print("Vero positvio: ",giusti2)

errori3=conteggi("dataset/maschere/maschereTrainThemoviedbSerieTv.pkl","dataset/clustering/clusteringTest.pkl","dataset/siti/test/testDiscotecalaziale.pkl")
print("Flaso positivo: ",errori3)

giusti3=conteggi("dataset/maschere/maschereTestDiscotecalaziale.pkl","dataset/clustering/clusteringTest.pkl","dataset/siti/test/testDiscotecalaziale.pkl")
print("Vero positvio: ",giusti3)

errorTot=errori1+errori2+errori3
giustiTot=giusti1+giusti2+giusti3
sites= contaSiti("dataset/siti/test/testThemoviedbSerieTv.pkl")+contaSiti("dataset/siti/test/testThemoviedbPersone.pkl")+contaSiti("dataset/siti/test/testDiscotecalaziale.pkl")
print(sites)
recall = giustiTot/sites
precision = giustiTot/(giustiTot+errorTot)
fmeasure = (2*recall*precision)/(recall+precision)
print("Recall: ",recall*100,"%")
print("Precision: ",precision*100,"%")
print("Fmeasure: ",fmeasure*100,"%")