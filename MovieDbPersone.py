from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
from htmldom import htmldom
import requests
import hashlib
import random
import time
from selenium.webdriver.common.keys import Keys
import pickle
import tegola



numeropagine=0
link="https://www.themoviedb.org/person?language=no-NO&page="
no_page=0
lista=[]
while (no_page<300):
    link="https://www.themoviedb.org/person?language=no-NO&page="+str(no_page)

    with webdriver.Edge(executable_path='edgedriver_win64/msedgedriver.exe') as driver:
                    driver.get(link)
                    soup = BeautifulSoup(driver.page_source, 'lxml')
                    ris=soup.findAll("div",attrs={"class":"item profile card"})
                    cont=0
                    try:
                        for x in ris:
                                    url="https://www.themoviedb.org"+x.find('a')['href']
                                    lista.append(tegola.pagetoByte(url))
                        driver.close    
                    except Exception:
                        pass    
    no_page+=1
a_file = open("themoviedbPersone.pkl", "wb")
pickle.dump(lista, a_file)
a_file.close()