from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
from htmldom import htmldom
import requests
import hashlib
import random
import time
from selenium.webdriver.common.keys import Keys
import pickle
import tegola



numeropagine=0
link="https://www.themoviedb.org/tv/on-the-air?language=no-NO"
no_page=50
lista=[]
while (no_page):
    with webdriver.Edge(executable_path='edgedriver_win64/msedgedriver.exe') as driver:
                    driver.get(link)
                    soup = BeautifulSoup(driver.page_source, 'lxml')
                    ris=soup.find("div",attrs={"class":"white_column no_pad"})
                    cont=0
                    try:
                        for page in ris.findAll('a', href=True):
                                if(cont==0):
                                        url="https://www.themoviedb.org"+page['href']
                                        lista.append(tegola.pagetoByte(url))
                                        cont=cont+1
                                elif(cont==2):
                                    cont=0
                                else:
                                    cont=cont+1 
                        link=url
                        driver.close 
                    except Exception:
                        pass    
    no_page-=1
a_file = open("themoviedbSerieTv.pkl", "wb")
pickle.dump(lista, a_file)
a_file.close()