from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
from htmldom import htmldom
import requests
import hashlib
import random
import time
from selenium.webdriver.common.keys import Keys
import pickle
import tegola
import os.path
import sklearn.model_selection


def trovaMaschere (nomefile1,nomefilemaschere):
    a_file=open(nomefile1, "rb")
    file1 = pickle.load(a_file)
    file2 = file1.copy()
    a_file.close()
    M=[]
    for x in file1:
        for y in file2:
            ris=[]
            i=0
            flag=0
            while (i<len(x)):
                if(x[i]==y[i]):
                    ris.append(x[i])
                elif(flag<2):
                    flag+=1
                    ris.append("*")
                i+=1
            if (len(ris)==len(x)):
                if (flag==0):
                    ris[4]="*"
                    ris[9]="*"
                    if (ris not in M):
                        M.append(ris)
                elif (flag<=2):
                    if (ris not in M):
                        M.append(ris)
    if (os.path.isfile(nomefilemaschere)):
                    a_file=open(nomefilemaschere, "rb")
                    output = pickle.load(a_file)
                    a_file.close()
                    for x in M:
                        output.append(x)
                    a_file = open(nomefilemaschere, "wb")
                    pickle.dump(output, a_file)
                    a_file.close()

    else:
                    a_file = open(nomefilemaschere, "wb")
                    pickle.dump(M, a_file)
                    a_file.close()

def clusterizzazione (nomefile,nomefile1,pathcluster):
    a_file=open(nomefile, "rb")
    maschere = pickle.load(a_file)
    a_file.close()
    a_file=open(nomefile1, "rb")
    file1 = pickle.load(a_file)
    a_file.close()
    dizionario=dict()
    numeromaschere=0
    for x in maschere:
        lista=[]
        for y in file1:
            i=0
            cont=0
            while (i<(len(x))):
                if (x[i]==y[i]):
                    cont+=1
                i+=1
            if(cont==(i-1))or(cont==(i-2)):
                lista.append(y)
        numeromaschere+=1

        if (len(lista)!=0):
            if(tuple(x) in dizionario):
                temp=dizionario[tuple(x)]
                temp.append(lista)
            else:
                    temp=lista 
            dizionario[tuple(x)]=temp

    if (os.path.isfile(pathcluster)):
                    a_file=open(pathcluster, "rb")
                    output = pickle.load(a_file)
                    print("output:",len(output))
                    a_file.close()
                    print("prima di funaiozne:",len(dizionario))
                    output=concatenaDizionari(output, dizionario)
                    a_file = open(pathcluster, "wb")
                    pickle.dump(output, a_file)
                    a_file.close()

    else:
                    a_file = open(pathcluster, "wb")
                    pickle.dump(dizionario, a_file)
                    a_file.close()


def concatenaDizionari(dizionario1,dizionario2):
    print("Dimensioni dizionari",len(dizionario1),len(dizionario2))
    for x in dizionario2.keys():
        if(x in dizionario1.keys()):
            temp=dizionario1[x]
            for y in dizionario2[x]:
                temp.append(y)
            dizionario1[x]=temp
        else:
            dizionario1[x]=dizionario2[x]
    print("dimensione dizionario:",len(dizionario1.keys()))
    return (dizionario1)

def diviTestAndSet(pathfile,pathTest,pathTrain):
        a_file=open(pathfile, "rb")
        setpagine = pickle.load(a_file)
        a_file.close()
        print(len(setpagine))
        random.shuffle(setpagine)
        test, train = sklearn.model_selection.train_test_split(setpagine)
        
        a_file=open(pathTest,"wb")
        pickle.dump(test, a_file)
        a_file.close()
        a_file=open(pathTrain,"wb")
        pickle.dump(train, a_file)
        a_file.close()

