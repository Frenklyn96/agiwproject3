from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
from htmldom import htmldom
import requests
import hashlib
import random
import time
from selenium.webdriver.common.keys import Keys
import pickle
import tegola
import mmh3

def hashValoreTegola (valore,indice):
    temp=""
    i=5
    while i:
        temp+=str(indice)
        i-=1
    somma = valore + mmh3.hash(str(valore),int(temp))
    return (int(str(abs(somma))[2:5]))

def min (lista,i):
    min=9999
    for x in lista:
        if(min>x[i]):
            min=x[i]
    return(min)

def tegolaRisultante(valori):
    j=0
    tegola=[]
    while (j<10):
        tegola.append(min(valori,j))
        j+=1
    return(tegola)



def pagetoByte(link):
    try:
        tags=[]
        with webdriver.Edge(executable_path='edgedriver_win64/msedgedriver.exe') as driver:
            driver.get(link)
            soup = BeautifulSoup(driver.page_source,"html.parser")    
        #Leggiamo tutti i tag della pagina e li dividiamo in gruppi da 10 tag in modo da definire 1 Tegola. 1 Tegola=10 Tag
            i=10
            for tag in soup.find_all():
                t=0
                for carattere in tag.name:
                    t= t+ord(carattere)
                if(i==0):
                    temp=[hashValoreTegola(t,i)]
                elif(i<10):
                    temp.append(hashValoreTegola(t,i))
                elif(i==10):
                    i=0
                    temp=[hashValoreTegola(t,i)]
                    tags.append(temp)
                i=i+1
        return (tegolaRisultante(tags[:-1]))
    except Exception:
        return(0)

