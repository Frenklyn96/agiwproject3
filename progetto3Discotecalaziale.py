from bs4 import BeautifulSoup
import urllib.request
import csv
from selenium import webdriver
import pandas as pd
import numpy as np
from htmldom import htmldom
import requests
import hashlib
import random
import time
from selenium.webdriver.common.keys import Keys
import pickle
import tegola


numeropagine=0
link="https://www.discotecalaziale.com/prodotti/prossime-uscite-pre-order-novit%C3%A0-musica?page="
no_page=0
lista=[]
while (no_page<100):
    link="https://www.discotecalaziale.com/prodotti/prossime-uscite-pre-order-novit%C3%A0-musica?page="+str(no_page)
    print(link)

    with webdriver.Edge(executable_path='edgedriver_win64/msedgedriver.exe') as driver:
                    driver.get(link)
                    soup = BeautifulSoup(driver.page_source, 'lxml')
                    ris=soup.findAll("div",attrs={"class":"scheda product"})
                    cont=0
                    for x in ris:
                            url="https://www.discotecalaziale.com"+x.find('a')['href']
                            lista.append(tegola.pagetoByte(url))
                    driver.close     
    no_page+=1
a_file = open("discotecalaziale.pkl", "wb")
pickle.dump(lista, a_file)
a_file.close()

a_file=open("discotecalaziale.pkl", "rb")
output = pickle.load(a_file)
print(output)